begin
  rhv_ems = $evm.vmdb(:ems).where(:name => 'RHV').first
  values_hash = {}
  values_hash['!'] = '** no vNIC profiles found **'
  vnic_profiles = Automation::Infrastructure::VM::RedHat::Utils.new(rhv_ems).vnic_profiles('Default')
  unless vnic_profiles.empty?
    values_hash['!'] = '-- select from list --'
      vnic_profiles.each do | profile |
      values_hash[profile[:id]] = profile[:name]
    end
  end

  list_values = {
    'sort_by'    => :value,
    'data_type'  => :string,
    'required'   => true,
    'values'     => values_hash
  }
  list_values.each { |key, value| $evm.object[key] = value }

 rescue => err
  $evm.log(:error, "[#{err}]\n#{err.backtrace.join("\n")}")
  exit MIQ_STOP

end
