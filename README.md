<h1> README </h1>

The business case was to implement parallel provisioning of vitual machines on the infrastructure provider.

The approach contains of ruby methods provided by Kevin Morey's CloudForms Essentials (https://github.com/ramrexx/CloudForms_Essentials).

Implemented process looks like this:

*  master service provisioning state machine contains build_vm_provision_request.rb and check_vm_provision_request.rb methods
*  build_vm_provision_request.rb method builds several service provisioning state machines based on the dialog values (eg. number of vm's) using a loop
*  each service provision request then triggers native VM provisiong state machine including the add_to_service.rb method which is adding the spawned virtual machines to master service (multiple_instance_initialization)
*  then check_vm_provision_request.rb is called from a master service provisioning state machine to cause it to wait for associated VM provisions to complete.
*  afterwards we end up with a service called from a generinc item Provisioning Entry Point which contains several virtual machines provision in parallel

<h3> Generic Item </h3>
![Generic Item](/images/genericitem.png)
<h3> Service provisioning state machine </h3>
![Service provisioning state machine](/images/servivestatemachine.png)
<h3> VM provisiong state machine </h3>
![VM provisiong state machine](/images/vmstatemachine.png)
<h3> Dialog </h3>
![Dialog](/images/dialog.png)
<h3> Service Requests </h3>
![Service Requests](/images/servicerequest.png)

